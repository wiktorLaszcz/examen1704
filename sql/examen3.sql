DROP DATABASE IF EXISTS examen1703;
CREATE DATABASE IF NOT EXISTS `examen1703` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE examen1703;

DROP TABLE IF EXISTS provinces;
CREATE TABLE provinces(
    id integer PRIMARY KEY,
    name varchar(100)
);


DROP TABLE IF EXISTS users;
CREATE TABLE users(
    id integer AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) not null,
    login varchar(100) not null,
    password varchar(100) not null,
    province_id integer,
    constraint roles_provinces_foreign foreign key (province_id) references provinces(id)
);


INSERT INTO provinces VALUES
(1, 'Álava'),
(2, 'Alicante'),
(08, 'Barcelona'),
(22, 'Huesca'),
(28, 'Madrid'),
(44, 'Teruel'),
(50, 'Zaragoza');


INSERT INTO users VALUES
(1, 'Juan Gracia', 'juan', 'secret1', 1),
(2, 'Manolo García', 'manolo', 'secret2', 50),
(3, 'Antonio Sánchez', 'antonio', 'secret3', 08),
(4, 'Roberto Miranda', 'roberto', 'secret4', 2),
(5, 'Fernando Martín', 'fernando', 'secret5', 28)
;
