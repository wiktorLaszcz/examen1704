<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.*"%>
<%@ include file = "header.jsp" %>
  <main>
  
<h1>Peliculas</h1>

<%! List <User> users;%>

<%
	users = (List<User>)request.getAttribute("users");
  if (users == null) {
	  users = new ArrayList<User>();
  }
%>

<table border = 1>
<tr>
	<th>ID</th>
	<th>Nombre</th>
	<th>Login</th>
	<th>Opciones</th>
</tr>
<% for (User item : users) {%>
  <tr>
  <td><%= item.getId() %></td>
  <td><%= item.getName() %></td>
  <td><%= item.getLogin() %></td>
  <td>
 	<a href="<%= request.getContextPath() %>/users/<%= item.getId() %>">ver</a>
	<a href="<%= request.getContextPath() %>/users/<%= item.getId() %>/remember">Recordar</a>
  	<a href="<%= request.getContextPath() %>/users/<%= item.getId() %>/delete">borrar</a>
  </td>
  </tr>
<% } %>
</table>
  </main>
<%@ include file = "footer.jsp" %>