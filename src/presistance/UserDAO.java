package presistance;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.User;



public class UserDAO {
	
	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/examen1703";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";
    private Connection connection = null;
	
    public UserDAO() {
    	
    	 try {
             Class.forName(DB_DRIVER).newInstance();
         } catch (Exception ex) {
             System.out.println("no se ha cargado el driver");
         }
         try {
             this.connection = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
             System.out.println("Conectado");

         } catch (SQLException ex) {
             System.out.println("Fallo Conexion");
             System.out.println("SQLException: " + ex.getMessage());
             System.out.println("SQLState: " + ex.getSQLState());
             System.out.println("VendorError: " + ex.getErrorCode());
         }
	}
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (connection != null) {
            connection.close();        
        }
    }
    
    public ArrayList <User> all() throws SQLException{
        ArrayList<User> users = new ArrayList<User>();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM users";

        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
            	User user = new User();
            	
            	user.setId(rs.getInt("id"));
            	user.setName(rs.getString("name"));
            	user.setLogin(rs.getString("login"));
            	user.setPassword(rs.getString("password"));
            	user.setProvinceId(rs.getInt("province_id"));
            	
            	users.add(user);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
        
        
        return users;
    }
    
    
    public void insert(User user) throws SQLException{
        PreparedStatement stmt = null;

        String sql = "INSERT INTO users (name, login, password, province_id) VALUES (?, ?, ?, ?);";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getLogin());
            stmt.setString(3, user.getPassword());
            stmt.setInt(4, user.getProvinceId());
            
            stmt.executeUpdate() ;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }
    }
    
    public Boolean find(User user) throws SQLException{
       
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM users WHERE login = ? and password = ?";
        User ulog = new User();

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getLogin());
            stmt.setString(2, user.getPassword());
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
            	ulog.setLogin(rs.getString("login"));
            }
            System.out.println(ulog.getLogin()+ "++++++++++++");
            if(ulog.getLogin().equals(user.getLogin())){
            	return true;
            }
            
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
        
        
        return false;
    }
    
    
    

}
