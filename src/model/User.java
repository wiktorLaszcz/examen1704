package model;

public class User {
	private Integer id;
	private String name;
	private String login;
	private String password;
	private Integer provinceId;
	
//	public User(Integer id, String name, String login, String password, Integer provinceId) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.login = login;
//		this.password = password;
//		this.provinceId = provinceId;
//	}
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public String getName() {
		return name;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public Integer getProvinceId() {
		return provinceId;
	}
	
	
}
