package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import presistance.UserDAO;


@WebServlet({"/users/*","/login"})
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public UserServlet() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        System.out.println(pieces[2]);

        if(length == 3 && pieces[2].equals("login")){
        	login(request, response);
        }
        
        if (length == 3 || pieces[3].equals("index")) {
        	
            index(request, response);
        }
        if(length == 4 && pieces[3].equals("create")){
        	create(request, response);
        }
        
        
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        if (length == 4 && pieces[3].equals("store") ) {
            store(request, response);
        }else if (length == 4 && pieces[3].equals("log")){
        	enter(request, response);
        }
	}
	
	private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/users.jsp");
        
        ArrayList<User> users = null;
        try {
            UserDAO dao= new UserDAO() ;
            users = dao.all();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
        
        request.setAttribute("users", users);
        dispatcher.forward(request, response);
    }
	
	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/create.jsp");
        dispatcher.forward(request, response);
    }
	
	private void store(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setLogin(request.getParameter("login"));
        user.setPassword(request.getParameter("password"));
        user.setProvinceId(Integer.parseInt(request.getParameter("province")));
        
        try{
        	UserDAO dao = new UserDAO();
            dao.insert(user);
        }catch (SQLException e) {
        	System.out.println("SQLException: " + e.getMessage());
		}
        response.sendRedirect(request.getContextPath() + "/users");
    }
	
	private void enter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        user.setLogin(request.getParameter("login"));
        user.setPassword(request.getParameter("password"));
        
        try{
        	UserDAO dao = new UserDAO();
            if(dao.find(user)){
            	HttpSession session = request.getSession(true);
                if (session.getAttribute("userlog") == null) {
                    session.setAttribute("userlog", new String());
                }else{
                	  session.setAttribute("userlog",user.getLogin());
                }
                response.sendRedirect(request.getContextPath() + "/users");
                return;
               
           }
        }catch (Exception e) {
        	System.out.println("SQLException: " + e.getMessage());
		}
        response.sendRedirect(request.getContextPath() + "/login");
    }
	
	private void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/login.jsp");
        dispatcher.forward(request, response);
    }
	
	private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
         String[] pieces = uri.split("/");
        int i = 0;
        for (String piece : pieces) {
            i++;
        }
        return pieces;
    }
	
	

}
